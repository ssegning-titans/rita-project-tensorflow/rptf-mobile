import 'package:beamer/beamer.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:rptf_mobile/environment_config.dart';
import 'package:rptf_mobile/services/navigation.dart';
import 'package:sentry/sentry.dart';
import 'package:wakelock/wakelock.dart';

Future<void> main() async {
  await Sentry.init(
    (options) {
      options.dsn = EnvironmentConfig.sentryDsn;
    },
    appRunner: initApp, // Init your App.
  );
}

void initApp() {
  WidgetsFlutterBinding.ensureInitialized();

  if (kDebugMode) {
    Wakelock.enable();
  }

  runApp(
    // For widgets to be able to read providers, we need to wrap the entire
    // application in a "ProviderScope" widget.
    // This is where the state of our providers will be stored.
    const ProviderScope(
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      routeInformationParser: BeamerParser(),
      routerDelegate: routerDelegate,
      backButtonDispatcher:
          BeamerBackButtonDispatcher(delegate: routerDelegate),
      title: 'RPTF',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
    );
  }
}
