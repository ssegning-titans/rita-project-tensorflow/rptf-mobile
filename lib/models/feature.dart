import 'package:json_annotation/json_annotation.dart';

part 'feature.g.dart';

@JsonSerializable()
class Feature {
  final String name;

  Feature({required this.name});

  /// Connect the generated [_$FeatureFromJson] function to the `fromJson`
  /// factory.
  factory Feature.fromJson(Map<String, dynamic> json) => _$FeatureFromJson(json);

  /// Connect the generated [_$FeatureToJson] function to the `toJson` method.
  Map<String, dynamic> toJson() => _$FeatureToJson(this);
}