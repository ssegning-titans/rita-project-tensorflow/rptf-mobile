import 'package:json_annotation/json_annotation.dart';
import 'package:rptf_mobile/models/feature.dart';

part 'qr_code.g.dart';

@JsonSerializable()
class QrCode {
  @JsonKey(name: '_id')
  final String id;
  final String name;
  final List<Feature> features;

  QrCode({required this.id, required this.name, required this.features});

  /// Connect the generated [_$QrCodeFromJson] function to the `fromJson`
  /// factory.
  factory QrCode.fromJson(Map<String, dynamic> json) => _$QrCodeFromJson(json);

  /// Connect the generated [_$QrCodeToJson] function to the `toJson` method.
  Map<String, dynamic> toJson() => _$QrCodeToJson(this);
}