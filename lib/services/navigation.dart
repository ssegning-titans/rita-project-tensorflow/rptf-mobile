import 'package:beamer/beamer.dart';
import 'package:rptf_mobile/screens/home_screen.dart';
import 'package:rptf_mobile/screens/scan_screen.dart';
import 'package:rptf_mobile/screens/upload_screen.dart';

final routerDelegate = BeamerDelegate(
  locationBuilder: RoutesLocationBuilder(
    routes: {
      // Return either Widgets or BeamPages if more customization is needed
      '/': (context, state, data) => const HomeScreenWidget(),
      '/scan': (context, state, data) => const ScanScreenWidget(),
      '/upload': (context, state, data) => const UploadScreenWidget(),
    },
  ),
);
