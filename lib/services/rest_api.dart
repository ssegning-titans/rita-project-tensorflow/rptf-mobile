import 'dart:io';

import 'package:dio/dio.dart' hide Headers;
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:http_parser/http_parser.dart';
import 'package:mime/mime.dart';
import 'package:rptf_mobile/environment_config.dart';
import 'package:rptf_mobile/models/qr_code.dart';
import 'package:sentry_dio/sentry_dio.dart';

class RestClient {
  final Dio _dio;
  final String baseUrl;

  RestClient(
    this._dio, {
    this.baseUrl = '/',
  });

  Future<dynamic> uploadFile(
    File file,
    List<String> features,
    String qrCodeId,
  ) async {
    final queryParameters = <String, dynamic>{
      r'features': features,
      r'qr-code-id': qrCodeId,
    };

    final data = FormData();
    data.files.add(
      MapEntry(
        'file',
        MultipartFile.fromFileSync(
          file.path,
          filename: file.path.split(Platform.pathSeparator).last,
          contentType: MediaType.parse(lookupMimeType(file.path)!),
        ),
      ),
    );

    final options = Options(
      method: 'POST',
      contentType: 'multipart/form-data',
    ).compose(
      _dio.options,
      '$baseUrl/api/v1/analysis/upload',
      queryParameters: queryParameters,
      data: data,
    );
    final result = await _dio.fetch(
      _setStreamType<dynamic>(options),
    );

    return result.data;
  }

  RequestOptions _setStreamType<T>(RequestOptions requestOptions) {
    if (T != dynamic &&
        !(requestOptions.responseType == ResponseType.bytes ||
            requestOptions.responseType == ResponseType.stream)) {
      if (T == String) {
        requestOptions.responseType = ResponseType.plain;
      } else {
        requestOptions.responseType = ResponseType.json;
      }
    }
    return requestOptions;
  }
}

class ApiService {
  final RestClient _restClient;

  ApiService(this._restClient);

  Future<void> uploadFile(QrCode qrCode, File file) async {
    await _restClient.uploadFile(
      file,
      qrCode.features.map((e) => e.name).toList(),
      qrCode.id,
    );
  }
}

final apiService = Provider((ref) {
  final dio = Dio()..interceptors.add(LogInterceptor())
      /*
      ..httpClientAdapter = Http2Adapter(
        ConnectionManager(
          idleTimeout: 10000,
          // Ignore bad certificate
          onClientCreate: (_, config) => config.onBadCertificate = (_) => true,
        ),
      )
      */
      ;

  dio.addSentry(
    captureFailedRequests: true,
  );

  return ApiService(
    RestClient(
      dio,
      baseUrl: EnvironmentConfig.baseUrl,
    ),
  );
});
