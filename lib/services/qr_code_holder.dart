import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:rptf_mobile/models/qr_code.dart';

@immutable
class _QrCodeState {
  final QrCode? qrCode;

  const _QrCodeState({required this.qrCode});

  factory _QrCodeState.init() {
    return const _QrCodeState(qrCode: null);
  }

  _QrCodeState copyWith({QrCode? qrCode}) {
    return _QrCodeState(
      qrCode: qrCode ?? this.qrCode,
    );
  }
}

class QrCodeNotifier extends StateNotifier<_QrCodeState> {
  QrCodeNotifier() : super(_QrCodeState.init());

  setQrCode(QrCode qrCode) {
    state = state.copyWith(qrCode: qrCode);
  }

  removeQrCode(QrCode qrCode) {
    state = state.copyWith(qrCode: qrCode);
  }
}

final _qrCodeNotifierProvider =
    StateNotifierProvider<QrCodeNotifier, _QrCodeState>((ref) {
  return QrCodeNotifier();
});

final qrCodeProvider = Provider.autoDispose((ref) {
  return ref.watch(_qrCodeNotifierProvider).qrCode;
});

final qrNotifierProvider = Provider.autoDispose((ref) {
  return ref.read(_qrCodeNotifierProvider.notifier);
});
