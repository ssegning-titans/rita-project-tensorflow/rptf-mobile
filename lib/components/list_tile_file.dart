import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:path/path.dart';

class ListTileFile extends HookWidget {
  final File file;
  final bool loading;

  const ListTileFile({
    super.key,
    required this.loading,
    required this.file,
  });

  @override
  Widget build(BuildContext context) {
    final name = useMemoized(() => basename(file.path), [file.path]);
    return ListTile(
      leading: Image.file(file),
      title: Text(name),
      trailing: loading ? const CircularProgressIndicator() : null,
    );
  }
}
