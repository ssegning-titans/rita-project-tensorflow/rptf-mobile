import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

class UploadSuccessModal extends HookWidget {
  final int number;

  const UploadSuccessModal({
    super.key,
    required this.number,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(24.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          const Text('Thank you!'),
          Text('$number images uploaded'),
          ListTile(
            title: const Text('Done'),
            onTap: () => Navigator.pop(context, AfterUploadAction.done),
            trailing: const Icon(Icons.arrow_forward_sharp),
          ),
          const Divider(),
          ListTile(
            title: const Text(
              'Send again?',
            ),
            onTap: () => Navigator.pop(context, AfterUploadAction.redo),
            trailing: const Icon(
              Icons.refresh,
            ),
          ),
        ],
      ),
    );
  }
}

enum AfterUploadAction { redo, done }
