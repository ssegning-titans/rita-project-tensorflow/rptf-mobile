import 'package:flutter/material.dart';
import 'package:rptf_mobile/models/qr_code.dart';

class ValidateScanModal extends StatelessWidget {
  final QrCode code;

  const ValidateScanModal({super.key, required this.code});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(24.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          const Text(
              'Found useful information in this QR Code. Proceed with that?'),
          ListTile(
            title: const Text('Proceed'),
            onTap: () => Navigator.pop(context, AfterScanFound.proceed),
            trailing: const Icon(Icons.arrow_forward_sharp),
          ),
          const Divider(),
          ListTile(
            title: const Text(
              'Cancel',
              style: TextStyle(color: Colors.red),
            ),
            onTap: () => Navigator.pop(context),
            trailing: const Icon(
              Icons.close,
              color: Colors.red,
            ),
          ),
        ],
      ),
    );
  }
}

enum AfterScanFound {
  proceed
}