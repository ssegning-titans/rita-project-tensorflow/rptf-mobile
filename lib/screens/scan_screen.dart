import 'dart:convert';

import 'package:beamer/beamer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:rptf_mobile/components/modal_wrapper.dart';
import 'package:rptf_mobile/components/validate_scan_modal.dart';
import 'package:rptf_mobile/models/qr_code.dart';
import 'package:rptf_mobile/services/qr_code_holder.dart';
import 'package:sentry/sentry.dart';

class ScanScreenWidget extends HookConsumerWidget {
  const ScanScreenWidget({super.key});

  @override
  Widget build(BuildContext context, ref) {
    final qrKey = useMemoized(() => GlobalKey());
    final qrController = useState<QRViewController?>(null);
    final modalOpen = useState<bool>(false);
    final isReady = useState<bool>(false);

    final navigate = useCallback((QrCode code) {
      ref.read(qrNotifierProvider).setQrCode(code);
      context.beamToNamed('/upload');
    }, []);

    final usefulContentFound = useCallback((QrCode code) async {
      if (modalOpen.value) {
        return;
      }

      modalOpen.value = true;

      final AfterScanFound result = await showCustomModalBottomSheet(
        context: context,
        builder: (c) => ValidateScanModal(code: code),
        containerWidget: (_, animation, child) => FloatingModal(
          child: child,
        ),
        expand: false,
      );

      if (result == AfterScanFound.proceed) {
        navigate(code);
        return;
      }

      modalOpen.value = false;
    }, [navigate, modalOpen.value]);

    final showError = useCallback(() {
      const snackBar = SnackBar(
        content: Text('Wrong QrCode scanned'),
        backgroundColor: Colors.red,
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }, [context]);

    final requestPermission = useCallback(() async {
      final status = await Permission.camera.request();
      if (status.isPermanentlyDenied) {
        isReady.value = false;
        return;
      }

      if (status.isGranted) {
        isReady.value = true;
        return;
      }
    }, []);

    useEffect(() {
      requestPermission();
      return () {
        qrController.value?.dispose();
      };
    }, []);

    final handleChange = useCallback((Barcode value) {
      try {
        final obj = jsonDecode(value.code!);
        final code = QrCode.fromJson(obj);
        usefulContentFound(code);
      } catch (e, s) {
        showError();
        Sentry.captureException(e, stackTrace: s);
      }

      return () {};
    }, [usefulContentFound, showError]);

    final onQRViewCreated = useCallback((QRViewController controller) {
      qrController.value = controller;
      controller.resumeCamera();
      controller.scannedDataStream.listen(handleChange);
    }, []);

    return Scaffold(
      appBar: AppBar(
        title: const Text('Scan'),
      ),
      body: isReady.value
          ? QRView(
              key: qrKey,
              onQRViewCreated: onQRViewCreated,
              overlay: QrScannerOverlayShape(),
            )
          : Center(
              child: Column(
                children: [
                  const Text('No permission to scan'),
                  ElevatedButton(
                    onPressed: requestPermission,
                    child: const Text('Request permission'),
                  )
                ],
              ),
            ),
    );
  }
}
