import 'package:beamer/beamer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class HomeScreenWidget extends HookConsumerWidget {
  const HomeScreenWidget({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final goToScan = useCallback(() => context.beamToNamed('/scan'), []);

    return Scaffold(
      appBar: AppBar(
        title: const Text('Home'),
      ),
      body: SafeArea(
        child: Column(
          children: [
            Expanded(
              child: ListView(
                controller: ScrollController(),
                children: const [
                  ListTile(
                    leading: Icon(Icons.arrow_forward_ios),
                    title: Text('First scan an Image'),
                  ),
                  ListTile(
                    leading: Icon(Icons.arrow_forward_ios),
                    title: Text('Then select files'),
                  ),
                  ListTile(
                    leading: Icon(Icons.arrow_forward_ios),
                    title: Text('Finally upload them'),
                  ),
                ],
              ),
            ),
            const Divider(),
            ListTile(
              onTap: goToScan,
              leading: const Icon(Icons.camera),
              title: const Text('Go to scan'),
              trailing: const Icon(Icons.arrow_forward),
            ),
          ],
        ),
      ),
    );
  }
}
