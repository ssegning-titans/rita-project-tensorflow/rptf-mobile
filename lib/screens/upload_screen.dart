import 'dart:io';

import 'package:beamer/beamer.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:rptf_mobile/components/list_tile_file.dart';
import 'package:rptf_mobile/components/modal_wrapper.dart';
import 'package:rptf_mobile/components/upload_success.dart';
import 'package:rptf_mobile/services/qr_code_holder.dart';
import 'package:rptf_mobile/services/rest_api.dart';
import 'package:sentry/sentry.dart';

class UploadScreenWidget extends HookConsumerWidget {
  const UploadScreenWidget({super.key});

  @override
  Widget build(BuildContext context, ref) {
    final qrCode = ref.watch(qrCodeProvider);
    final pickedFiles = useState<List<File>>([]);
    final loading = useState(false);

    final upload = useCallback((File file) async {
      if (qrCode == null) {
        return;
      }

      await ref.read(apiService).uploadFile(qrCode, file);
    }, [qrCode]);

    final goToHome = useCallback(() => context.beamToReplacementNamed('/'), []);

    final showError = useCallback((e) {
      const snackBar = SnackBar(
        content: Text('Error uploading uploading files'),
        backgroundColor: Colors.red,
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }, [context]);

    final reset = useCallback(() {
      pickedFiles.value = [];
    }, []);

    final uploadAll = useCallback(() async {
      loading.value = true;

      try {
        final results =
            await Future.wait(pickedFiles.value.map((e) => upload(e)));

        loading.value = false;
        final AfterUploadAction result = await showCustomModalBottomSheet(
          context: context,
          builder: (c) => UploadSuccessModal(number: results.length),
          containerWidget: (_, animation, child) => FloatingModal(
            child: child,
          ),
          expand: false,
          isDismissible: false,
        );

        if (result == AfterUploadAction.redo) {
          reset();
        } else {
          goToHome();
        }
      } catch (e, s) {
        loading.value = false;
        showError(e);
        Sentry.captureException(e, stackTrace: s);
      }
    }, [pickedFiles.value, reset, upload, showError, goToHome]);

    final pickFile = useCallback(() async {
      final result = await FilePicker.platform.pickFiles(
        allowMultiple: true,
        allowCompression: true,
        type: FileType.image,
      );

      if (result != null) {
        final files = result.paths
            .where((path) => path != null)
            .map((path) => File(path!));

        pickedFiles.value = [...pickedFiles.value, ...files];
      }
    }, []);

    if (qrCode == null) {
      return Scaffold(
        body: Center(
          child: ElevatedButton(
            onPressed: goToHome,
            child: const Text('No QR Code scanned. Go back'),
          ),
        ),
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text('Pick photos'),
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(vertical: 24.0),
        child: Column(
          children: [
            Expanded(
              child: ListView.separated(
                itemBuilder: (context, index) => ListTileFile(
                  file: pickedFiles.value[index],
                  loading: loading.value,
                ),
                itemCount: pickedFiles.value.length,
                shrinkWrap: true,
                separatorBuilder: (_, idx) => const Divider(),
              ),
            ),
            const Divider(),
            ListTile(
              onTap: !loading.value ? pickFile : null,
              title: const Text('Pick files'),
              trailing: !loading.value
                  ? const Icon(Icons.add)
                  : const CircularProgressIndicator(),
            ),
            ListTile(
              onTap: !loading.value ? uploadAll : null,
              title: const Text('Upload'),
              trailing: !loading.value
                  ? const Icon(Icons.upload)
                  : const CircularProgressIndicator(),
            ),
          ],
        ),
      ),
    );
  }
}
