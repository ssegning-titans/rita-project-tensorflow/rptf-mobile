# RPTF Mobile

Rita Project TensorFlow

## Commands

- Generate splash screen
  ```bash
  flutter pub run flutter_native_splash:create
  ```
  
- Generate splash screen
  ```bash
  flutter pub run flutter_launcher_icons
  ```

- Generate env config
  ```bash
  flutter pub run environment_config:generate -b <backend-url> -s <sentry-dsn>
  ```

